
#Variable donde se guarda el registro diario de directorios

DIR=/home/$USER/tmp/directorios.txt


 
echo "Al dia de hoy $(date +%d-%m-%Y)" a las $(date +%r) | tee -a $DIR
echo "los directorios y archivos existentes del usuario $USER son los siguientes:" | tee -a $DIR
echo""
ls /home/$USER | tee -a $DIR
echo""
echo "Este script se ejecuto en la 'tty' $(tty) con ip" 
hostname -I
echo""
echo "Se guarda registro de directorios existentes en $DIR"
