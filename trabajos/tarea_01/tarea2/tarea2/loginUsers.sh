#!/bin/bash

#CREO VARIABLE PARA REGISTRO DE USUARIOS
LOGINALL=$HOME/login-all.txt 

#CREO VARIABLE PARA REGISTRO DEL USUARIO INDIVIDUAL
LOGINUSER=$HOME/$USER-login.txt

lastlog --time 30 > $LOGINALL

lastlog --user $USER > $LOGINUSER


echo "Se crea documento de inicio de sesion del usuario en $LOGINUSER" 
echo ""
echo "Se crea documento de inicio de sesion de todos los usuarios en $LOGINALL"
echo ""
