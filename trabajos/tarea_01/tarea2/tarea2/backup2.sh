#!/bin/bash

#DECLARO LAS VARIABLES QUE VOY A UTILIZAR

DIR=$HOME
ERRORES=$HOME/errores-t2.txt
LOGBACKUP=$HOME/logbackup.txt
DATE=$(date +%Y-%m-%d-%H%M%S)
DESTINO=$HOME/backups

#ENVIO MENSAJE DE INICIO DE BACKUP
echo "Se inicia el proceso de back up a las $(date +%r)" | tee -a $LOGBACKUP
echo ""

#VERIFICO QUE EL DIRECTORIO $HOME/BACKUPS EXISTA Y EN CASO DE QUE NO, LO CREO Y ENVIO
#MENSAJE DE CREACION DE DIRECTORIO

test -d $HOME/backups || mkdir $HOME/backups && echo "Directorio 'backups' creado" 2>$ERRORES | tee -a $LOGBACKUP  
echo "" 

#CREO ARCHIVO TAR CON EL BACKUP, Y LO MUEVO A LA VARIABLE QUE INDICA EL DIRECTORIO SELECCIONADO

su --c "tar --exclude=$HOME/Scripts/tarea2 -czvf backup-$USER-$DATE.tar $HOME && mv backup-$USER-$DATE.tar $DESTINO" root\
&& echo "Se creo el backup correctamente" 2>$ERRORES | tee -a $LOGBACKUP

echo ""

#ENVIO MENSAJE DE QUE EL PROCESO DE BACKUP FINALIZO CORRECTAMENTE

echo "Finaliza el proceso de back up a las $(date +%r)"

echo ""

