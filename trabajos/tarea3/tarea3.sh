#!/bin/bash

#Instituto ISTEA
#Materia: Programacion
#Profesor: Sergio Pernas
#Alumno: Alejandro Areco
#Tarea: 3


#Evaluo si el actual usuario es root o no, y envio mensaje de acuerdo al resultado
if [ $UID -ne  0 ]; then
	echo "ATENCION: Este programa solo puede ser utilizado por 'root'"
	exit 1
fi

echo ""

#Envio mensaje de bienvenida al usuario y doy instrucciones de uso

MSJ="Bienvenido $USER\n
\tPrograma Adrministradr  de Archivos, Directorios y Permisos.\n 
\tEjecute:\n
\t$0 seguido de las instrucciones para operar\n


Instrucciones:\n
\tchmod\trwx\tArchivo o Directorio\t--> Cambia Permiso a archivos o directorios\n
\tchown\trwx\tDirectorio o Ficheros\t--> Cambia Dueño a Directorios o Ficheros\n
\tgroupadd\tNombre de Grupo\t--> Para agregar grupo\n
\tgroupdel\tNombre de Grupo\t--> Para eliminar grupo\n
\tcat\t/etc/group\t --> Para ver grupos existentes\n
\tfind\tpath\t-perm\t-'tipo_de_permiso'\t--> Para buscar archivos por tipo de permisos\n"


#Indico que si el programa no recibe intrucciones, las repita
if [ -z "$*" ]; then
	echo -e $MSJ
	exit 1
fi


#Instrucciones
# 1
##Pregunto al usuario si desea continuar
echo "Desea continuar? (s/n)"
read input

## Si la instruccion recibida coincide, la cantidad de caracteres
## es igual o mayor que 2 y el 'input' (confirmacion de usuario)
## es y (si), ejecuta la instruccion

if [[ $1 == chmod && $# -ge 2 && "$input" == s  ]]; then

	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	chmod $ARCH && echo "Operacion Realizada con exito"
	exit
fi	


#2
if [[ $1 == chown && $# -ge 3 && "$input" == s  ]]; then

	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	chown $ARCH && echo "Operacion Realizada con exito"
	exit
fi

#3
if [[ $1 == groupadd && "$input" == s ]]; then

	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	groupadd $ARCH && echo "Operacion Realizada con exito"
	exit
fi

#4
if [[ $1 == groupdel && "$input" == s ]]; then
	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	groupdel $ARCH && echo "Operacion Realizada con exito"
	exit
fi

#5
if [[ $1 == cat && $# -ge 2 && "$input" == s ]]; then

	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	cat $ARCH && echo "Operacion Realizada con exito"
	exit
fi

#6
if [[ $1 == find && $# -ge 3 && "$input" == s ]]; then
	ARCH="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"

	find $ARCH && echo "Operacion Realizada con exito"
	exit
fi
