#!/bin/bash

baja_sitio () {

echo "Ingrese el nombre del sitio a dar de baja"

read nombre

rm /etc/apache2/sites-available/"$nombre".com.conf
rm -r /var/www/"$nombre".com

#REINICIO APACHE PARA APLICAR CAMBIOS

systemctl reload apache2

}
