#!/bin/bash

#Instituto ISTEA
#Materia: Programacion
#Profesor: Sergio Pernas
#Alumno: Alejandro Areco
#Tarea: 5

#VERIFICIO SI EL USUARIO ES 'ROOT'. EN CASO DE NO SERLO, ENVIO MENSAJE DE AVISO

if [ $UID -ne 0 ]; then
	echo "ATENCION: este programa solo puede ser ejecutado por usuario 'root'"
	exit 1
fi

#VERIFICO SI APACHE YA ESTA INSTALADO
#SI NO ESTA INSTALADO, PREGUNTO SI SE QUIERE INSTALAR

sudo which apache2 &> /dev/null

if [ $? -eq 0 ]; then
	echo "Apache esta instalado. Presione 'enter' para continuar"
else
	echo "Apache no esta instalado. Desea instalarlo? [Y/n]"
fi

read INSTALAR

if [ "$INSTALAR" == "Y" ]; then
	sudo apt update && apt install apache2
else 
	echo "Gracias por utilizar el programa"
fi

echo "Desea instalar componentes de stack? [Y/n] "

read STACK

if [ "$STACK" == "Y" ]; then
	echo "Seleccione el componente a instalar"
	while :;do
		echo "1) MySql Server"
		echo "2) Php"
		echo "3) Todos los componentes"
		echo "4) Salir"

	read -p "Ingrese numero de opcion: " OPC

	case $OPC in
		1) 
			clear
			apt install default-mysql 2> /dev/null
			clear
			;;
		2) 	clear 
			php libapache2-mod-php php-mysql 2> /dev/null
			echo "<?php
    			phpinfo();
    			phpinfo(INFO_MODULES);
			?>" > /var/www/html/info.php
			IP=$(hostname -I)
			echo "ingrese a $IP/info.php para testear 'php'"
			break
			clear
			;;
		3)	clear
			apt install default-mysql-server php libapache2-mod-php php-mysql 2> /dev/null
			clear
			;;
		4)      break
			;;
	esac
	clear
	done
fi

echo "Para que 'apache' aplique los cambios realizados, debe reiniciarlo."
echo "Desea reiniciar 'apache' ahora? [Y/n]"

read RTA

if [ "$RTA" == "Y" ]; then
	systemctl restart apache2
else 
	echo "Los cambios realizados no se aplicaran hasta que apache no se reinicie"
fi

echo ""
echo "// Administracion de Sitios //"
echo ""

source altasitio.sh

while :;do
echo "Opciones:"
echo "1) Dar de Alta sitio nuevo"
echo "2) Dar de Baja sitio"
echo "3) Salir"

read -p "Ingrese numero de opcion: " OPT

case $OPT in
    	1)
            	clear
            	alta_sitio
            	clear
            	;;
	2)
		clear
		echo "TENGO QUE AGRGEAR OPCION PARA DAR DE BAJA SITIO"
#
#		read -p "Ingrese nueva direccion ip:" IP
#
#		read -p "Ingrese nuevo hostname:" HSTN
#		$HSTN
		clear
		;;
	3) 
		break
		;;
esac
clear
done

echo ""

echo "// Modificar IP y HOSTNAME //"

echo ""

while :;do
echo "Opciones:"
echo "1) Modificar Hostname"
echo "2) Modificar IP"
echo "3) Salir"

read -p "Ingrese numero de opcion: " SELECT

#GUARDO DEFAULT GATEWAY EN VARIABLE

GTW=$(ip r | grep default | cut -d " " -f 3)

case $SELECT in
    	1)
            	clear
            	echo "Ingrese el nuevo Hostname: "
	        read name
		hostnamectl set-hostname $name	
		echo "El hostname se ha modificado a :"
		hostname 
		break
		echo "ADVERTENCIA: los cambios realizados tomara efecto al reiniciar el servidor"
		echo "Desea reiniciar el servidor ahora? [Y/n]"

		read REBOOT

		if [ "$REBOOT" == "Y" ]; then
		sudo reboot
	else
		echo "Los cambios realizados no se aplicaran hasta que el servidor no se reinicie"
fi
            	clear
            	;;
    	2)
            	clear
		sed "12d" interfaces > interfaces
		NIF=$(ls -l /sys/class/net/ | grep -v virtual | cut -d "/" -f 7 | sed "1d")
		echo "Ingrese ip: "
		read IP
		echo "iface $NIF inet static
    		address $IP
    		netmask 255.255.255.0
    		gateway $GTW" >> /etc/network/interfaces
	      	echo "Configuracion de Interfaz de red Actual:"
		cat /etc/network/interfaces
		break
		ifdown $NIF
		ifup $NIF
		clear
           	;;
	3)
		break
		;;
esac
clear
done

echo "ADVERTENCIA: los cambios realizados tomara efecto al reiniciar el servidor"
echo "Desea reiniciar el servidor ahora? [Y/n]"

read REBOOT

if [ "$REBOOT" == "Y" ]; then
	sudo reboot
else
	echo "Los cambios realizados no se aplicaran hasta que el servidor no se reinicie"
fi
