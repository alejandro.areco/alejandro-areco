#!/bin/bash

#DOY DE ALTAR AL SITIO

alta_sitio () {

echo "Ingrese el nombre del sitio a dar de alta"

read nombre

echo "<VirtualHost *:80>
       		ServerAdmin webmaster@localhost
    		DocumentRoot /var/www/"$nombre".com
		ServerName "$nombre".com  
		ErrorLog ${APACHE_LOG_DIR}/error.log
    		CustomLog ${APACHE_LOG_DIR}/access.log combined
    
	</VirtualHost>"  > /etc/apache2/sites-available/"$nombre".com.conf

#GUARDO EL NOMBRE DEL ARCHIVO EN UNA VARIABLE	

SITIO="$nombre".com.conf

#CREO EL SITIO JUNTO A UN TEMPLATE INDEX

mkdir /var/www/"$nombre".com

echo "<h1> Sitio "$nombre" creado con exito </h1>" > /var/www/"$nombre".com/index.html 

#ACTIVO SITIO CREADO

a2ensite $SITIO

#REINICIO APACHE PARA QUE TOME LOS CAMBIOS REALIZADOS

systemctl reload apache2 

echo "Apache reiniciado"

#EDITO FICHERO 'HOSTS" PARA SIMULAR DOMINIO

IP=$(hostname -I) 

echo $IP "$nombre".com >> /etc/hosts

}

