#!/bin/bash

migrar_sitio(){

#ENVIO MSJ E INTRUCCIONES PARA PREPARAR SERVIDOR REMOTO
	
echo "IMPORTANTE: recuerde habilitar sesión por ssh como usuario root en servidor de destino antes de migrar el sitio"
echo "Siga las siguientes instrucciones en el servidor de destino"
echo "1 - vim /etc/ssh/sshd_config"
echo "2 - Descomentar ’PermitRootLogin prohibit-password’"
echo "3 - Cambiar por ‘PermitRootLogin yes’"
echo "4 - Guardar cambios en el archivo ‘:wq!’"
echo "5 - Reinicie servicio ‘service ssh restart’"

echo""

#LISTO SITIOS ACTIVOS

echo "Listado actual de archivos de sitios"
ls /etc/apache2/sites-available

echo""
echo "Indique el nombre del archivo a migrar"

read ARCHIVO

echo ""
echo "Indique la ip de servidor de destino"

read DESTINO

scp /etc/apache2/sites-available/"$ARCHIVO" root@"$DESTINO":/etc/apache2/sites-available

echo "Desea activar sitio remoto? [Y/n]"

read RTA

if [ "$RTA" == "Y" ]; then
	ssh root@"$DESTINO" 'a2ensite "'$ARCHIVO'"'
	ssh root@"$DESTINO" 'systemctl reload apache2'
else 
	echo "sitio no activado en servidor remoto"
fi
}
