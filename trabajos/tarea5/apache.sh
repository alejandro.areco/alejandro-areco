#!/bin/bash

#Instituto ISTEA
#Materia: Programacion
#Profesor: Sergio Pernas
#Alumno: Alejandro Areco
#Tarea: 5

#VERIFICIO SI EL USUARIO ES 'ROOT'. EN CASO DE NO SERLO, ENVIO MENSAJE DE AVISO

if [ $UID -ne 0 ]; then
	echo "ATENCION: este programa solo puede ser ejecutado por usuario 'root'"
	exit 1
fi



echo "// Modificar IP y HOSTNAME //"

echo ""

while :;do
echo "Opciones:"
echo "1) Modificar Hostname"
echo "2) Modificar IP"
echo "3) Continuar"

read -p "Ingrese numero de opcion: " SELECT

#GUARDO DEFAULT GATEWAY EN VARIABLE

GTW=$(ip r | grep default | cut -d " " -f 3)

case $SELECT in
    	1)
            	clear
            	echo "Ingrese el nuevo Hostname: "
	        read name
		hostnamectl set-hostname $name
		echo "El hostname se ha modificado a :"
		hostname
		break
		echo "ADVERTENCIA: los cambios realizados tomara efecto al reiniciar el servidor"
		echo "Desea reiniciar el servidor ahora? [Y/n]"

		read REBOOT

		if [ "$REBOOT" == "Y" ]; then
		sudo reboot
	else
		echo "Los cambios realizados no se aplicaran hasta que el servidor no se reinicie"
fi
            	clear
            	;;
    	2)
            	clear
		sed "12d" interfaces > interfaces
		NIF=$(ls -l /sys/class/net/ | grep -v virtual | cut -d "/" -f 7 | sed "1d")
		echo "Ingrese ip: "
		read IP
		echo "iface $NIF inet static
    		address $IP
    		netmask 255.255.255.0
    		gateway $GTW" >> /etc/network/interfaces
	      	echo "Configuracion de Interfaz de red Actual:"
		cat /etc/network/interfaces
		break
		ifdown $NIF
		ifup $NIF
		clear
           	;;
	3)
		break
		;;
esac
clear
done

echo "ADVERTENCIA: los cambios realizados tomara efecto al reiniciar el servidor"
echo "Desea reiniciar el servidor ahora? [Y/n]"

read REBOOT

if [ "$REBOOT" == "Y" ]; then
	sudo reboot
else
	echo "Los cambios realizados no se aplicaran hasta que el servidor no se reinicie"
fi



#VERIFICO SI APACHE YA ESTA INSTALADO
#SI NO ESTA INSTALADO, PREGUNTO SI SE QUIERE INSTALAR

echo ""
echo "// Instalacion de Programas //"
echo ""

sudo which apache2 &> /dev/null

if [ $? -eq 0 ]; then
	echo "Apache esta instalado. Presione 'enter' para continuar"
else
	echo "Apache no esta instalado. Desea instalarlo? [Y/n]"
fi

read INSTALAR

if [ "$INSTALAR" == "Y" ]; then
	sudo apt update && apt install apache2
else 
	echo "// Gestor de Apache iniciado //"
fi

echo""

echo "Desea instalar componentes de stack? [Y/n] "

read STACK

if [ "$STACK" == "Y" ]; then
	echo "Seleccione el componente a instalar"
	while :;do
		echo "1) MySql Server"
		echo "2) Php"
		echo "3) Todos los componentes"
		echo "4) Continuar"

	read -p "Ingrese numero de opcion: " OPC

	case $OPC in
		1) 
			clear
			apt install default-mysql
			clear
			;;
		2) 	clear 
			php libapache2-mod-php php-mysql
			echo "<?php
    			phpinfo();
    			phpinfo(INFO_MODULES);
			?>" > /var/www/html/info.php
			IP=$(hostname -I)
			echo "ingrese a $IP/info.php para testear 'php'"
			break
			clear
			;;
		3)	clear
			apt install default-mysql-server php libapache2-mod-php php-mysql 2> /dev/null
			clear
			;;
		4)      break
			;;
	esac
	clear
	done
fi

echo "Para que 'apache' aplique los cambios realizados, debe reiniciarlo."
echo "Desea reiniciar 'apache' ahora? [Y/n]"

read RTA

if [ "$RTA" == "Y" ]; then
	systemctl restart apache2
else 
	echo "Los cambios realizados no se aplicaran hasta que apache no se reinicie"
fi


echo ""

echo "// Desinstalar  Programas //"

echo ""

echo "Desea eliminar componentes de stack? [Y/n] "

read DESINSTALAR

if [ "$DESINSTALAR" == "Y" ]; then
	echo "Seleccione el componente a instalar"
	while :;do
		echo "1) MySql Server"
		echo "2) Php"
		echo "3) Todos los componentes"
		echo "4) Continuar"

	read -p "Ingrese numero de opcion: " SELECT

	case $SELECT in
		1)
			clear
			apt-get remove --purge mysql-server.*
			clear
			;;
		2) 	clear
			apt-get remove --purge php7.*
			clear
			;;
		3)	clear
			apt-get remove --purge mysql-server.* php7.*
			clear
			;;
		4)      break
			;;
	esac
	clear
	done
fi

echo "Para que 'apache' aplique los cambios realizados, debe reiniciarlo."
echo "Desea reiniciar 'apache' ahora? [Y/n]"

read RTA

if [ "$RTA" == "Y" ]; then
	systemctl restart apache2
else
	echo "Los cambios realizados no se aplicaran hasta que apache no se reinicie"
fi

echo ""

echo "// Administracion de Sitios //"

echo ""

source altasitio.sh
source bajasitio.sh
source log.sh

while :;do
echo "Opciones:"
echo "1) Dar de Alta sitio nuevo"
echo "2) Personalizar log de acceso por sitio" 
echo "3) Dar de Baja sitio"
echo "4) Continuar"

read -p "Ingrese numero de opcion: " OPT

case $OPT in
    	1)
            	clear
            	alta_sitio
		echo "Sitios creados: "
		echo ""
		ls /etc/apache2/sites-available
		break
            	;;
	2)	
		clear
		mod_log
		;;
	3)	
		clear
		baja_sitio
		clear
		;;
	4) 
		break
		;;
esac
clear
done

echo ""
echo "// Migrar Sitios //"
echo ""

source migrarsitio.sh

while :;do
echo "Opciones:"
echo "1) Migrar sitio"
echo "2) Continuar"

read -p "Ingrese numero de opcion: " OPT

case $OPT in 
	1) 
		clear
		migrar_sitio
		break
		clear
		;;
	2)	
		break
		;;
esac
clear
done

source migrarsitio.sh

echo ""
echo "// Migrar Archivos de Sitios //"
echo ""

source migrararchivos.sh

while :;do
echo "Opciones:"
echo "1) Migrar Archivos de Sitio"
echo "2) Salir"

read -p "Ingrese numero de opcion: " OPT

case $OPT in
	1)
		clear
		migrar_archivo
		break
		clear
		;;
	2)
		break
		;;
esac
clear
done

clear

echo "Gracias por utilizar el programa gestor de Apache"
echo "Vuelva a ejecutar el programa si desear realizar mas operaciones"

