#!/bin/bash

#Instituto ISTEA
#Materia: Programacion
#Profesor: Sergio Pernas
#Alumno: Alejandro Areco
#Tarea: 4

#VERIFICIO SI EL USUARIO ES 'ROOT'. EN CASO DE NO SERLO, ENVIO MENSAJE DE AVISO

if [ $UID -ne 0 ]; then
	echo "ATENCION: Este programa solo puede ser utilizado por 'root'"
	exit 1
fi

#INSTALO UFW Y ENVIO MENSAJE DE ESTADO DE UFW LUEGO DE LA INSTALACION
#PARA EVITAR QUE DESCONECTE SESIONES POR SSH, HABILITO EL PUERTO POR DEFAULT

dpkg -s ufw > /dev/null && echo "'$(dpkg -s ufw | grep Package && dpkg -s ufw | grep Status)'" ||\
	echo "Se instalara el paquete 'ufw'"||\ 
	echo "Desea continuar? [Y/n]"
	read input
	if [ "$input" == Y ]; then
	su -c "apt update > /dev/null && apt install ufw" root 
	echo ""
	echo "Estado actual de 'ufw':"
	sudo ufw status 
else
	echo "Operacion suspendida por el usuario, vuelva a ejecutar el programa"
	exit 1
	fi
	echo "Desea activar ufw?[Y/n]"
	read activar
	if [ "$activar" == Y ]; then
		echo "Puerto escuchando conexiones ssh"
	        grep -i port /etc/ssh/sshd_config
		echo "Permitiendo conexiones ssh"
		sudo ufw allow 8822/tcp
		sudo ufw allow OpenSSH	
       		echo ""
		echo "Habilitando ufw"
		sudo ufw enable
	echo "'$(sudo ufw status)'"
	fi
echo""

ESC="Operacion suspendida por $USER, vuelva a ejecutar el programa"

#DECLARO OPCIONES A UTILIZAR EN EL CICLO FOR

abm=("Permitir","Denegar","Eliminar","Modificar","Cancelar")

#PREGUNTO AL USUARIO SI DESEAR VER LAS OPCIONES DE ABM DISPONIBLES

echo "Desea ver las opciones de ABM disponibles? [Y/n]"
read input
if [ "$input" == Y ]; then
	# echo "Seleccione una operacion a realizar"
 for i in "${abm}"
  do
 echo $i
done
else
echo -e $ESC
exit 1
fi

echo ""

#MUESTRO LAS OPCIONES DISPONIBLES DE ABM

echo "Seleccione una operacion a realizar

1) Permitir conexiones a puertos
2) Denegar conexiones a puertos
3) Eliminar regla
4) Modificar posicion de regla
5) Cancelar"

echo ""

read OPERACION

echo ""

if [ "$OPERACION" == "1" ]; then
	echo "opcion 1 seleccionada"
	echo "ingrese una argumento (protocolo o puerto)"
	read input
	sudo ufw allow "$input"
	sudo ufw status
elif [ "$OPERACION" == "2" ]; then
	echo "opcion 2 seleccionada"
	echo "ingrese una argumento (protocolo o puerto)"
	read input
	sudo ufw deny "$input"
	sudo ufw status
elif [ "$OPERACION" == "3" ]; then
	echo "opcion 2 seleccionada"
	echo "Estas son las reglas actuales de 'ufw'"
	sudo ufw status numbered
	echo "ingrese el numero de de regla '[n]'que desea eliminar"
	read input
	sudo ufw delete "$input"
	sudo ufw status
elif [ "$OPERACION" == "4" ]; then
	echo "posicionamiento actual de reglas"
	sudo ufw status numbered
	echo "ingrese el numero de regla a modificar"
	read input
	sudo ufw delete "$input" | tee /tmp/posicionRegla.txt
	VAR=$(cut -d ':' -f2 /tmp/posicionRegla.txt | head -n2)
	echo "ingrese la posicion en la que desea agregar la regla"
	read input
	sudo ufw insert $input $VAR
	sudo ufw status numbered
elif [ "$OPERACION" == "5" ]; then
	echo -e $ESC
exit 1
fi

#MUESTRO LAS OPCIONES POR DEFAULT 

echo "Este programa permite modificar las politicas definidas por defecto en 'ufw' definidas:"

echo""

cat /etc/default/ufw

echo ""

#PREGUNTO AL USUARIO SI DESEA MODIFICAR LAS POLITICAS POR DEFECTO

echo "Desea modificar las default policies? [Y/n]"

echo""

read input

if [ $input == "Y" ]; then
       echo "Seleccione la default policy que desee aplicar
       	    1) Aceptar por defecto conexiones entrantes
    	    2) Denegar por defecto conexiones salientes "
    else
	    echo $ESC
	    exit 1
fi

read SELECT

if [ "$SELECT" == "1" ]; then
	sudo ufw default allow incoming
elif [ "SELECT" == "2" ]; then
	sudo ufw default deny outgoing
fi

echo""

echo "Politicas por default actuales"
echo""
cat /etc/default/ufw

echo""

#SE REALIZA BACKUP DE LAS REGLAS ACTUALES Y SE GUARDAR EN ARCHIVO DE TEXTO

echo "A continuacion, se muestran las reglas actuales y se realizar backup de las mismas"

sudo ufw status numbered > bkpReglas.txt

while read line; do
	echo $line;
done < bkpReglas.txt

echo ""

echo "Puede encontrar una copia de las reglas existentes en el archivo 'bkpReglas.txt'"

#PREGUNTO SI SE DESEA QUE UFW INICIE CON EL SISTEMA

echo "Desea que 'ufw' incie con el sistema [Y/n]"

echo""

read input

if [ $input == "Y" ]; then
	sudo ufw enable

elif [ $input == "n" ]; then
	sudo ufw disable
fi
